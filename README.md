## 资源
* [code repository](https://gitlab.com/goddy-basic/spring-security-oauth2-jwt)
* [api doc](https://documenter.getpostman.com/view/4234097/RWEjoxKx)
* [blog](https://www.jianshu.com/p/6ca14ce04c09)
## 技术栈
* spring boot 2.0
* spring security oauth 2.0
* spring jpa
* keytool
* jwt
* mysql
## 依赖工具
* lombok
    * 参考文档：https://blog.csdn.net/zhglance/article/details/54931430
* 阿里代码规范插件（非必须）
    * 参考文档：https://github.com/alibaba/p3c