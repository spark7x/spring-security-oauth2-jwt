package com.goddy.oauth2.repository;

import com.goddy.oauth2.model.DO.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Goddy
 * @date Create in 下午12:04 2018/6/25
 * @description
 */
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * 按名称查询用户
     * @param username
     * @return
     */
    User findByUsername(String username);
}
