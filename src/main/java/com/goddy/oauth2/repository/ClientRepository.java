package com.goddy.oauth2.repository;

import com.goddy.oauth2.model.DO.Client;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Goddy
 * @date Create in 下午12:04 2018/6/25
 * @description
 */
public interface ClientRepository extends JpaRepository<Client, Long> {

    /**
     * 按
     * @param clientId
     * @return
     */
    Client findByClientId(String clientId);
}
