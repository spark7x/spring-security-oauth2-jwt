package com.goddy.oauth2.model.DO;

import lombok.Data;

import javax.persistence.*;

/**
 * @author Goddy
 * @date Create in 上午11:41 2018/6/25
 * @description
 */
@Data
@Entity
@Table(name = "oauth_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /** 用户名 */
    @Column(nullable = false, unique = true)
    private String username;

    /** 密码 */
    private String password;

    /** 是否可用 */
    private Boolean enabled;

    /** 是否被锁 */
    private Boolean noLocked;

    /** 权限 */
    private String authorities;
}
