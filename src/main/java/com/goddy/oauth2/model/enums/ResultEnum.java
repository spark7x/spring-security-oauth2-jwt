package com.goddy.oauth2.model.enums;

import lombok.Getter;

/**
 * @author Goddy
 * @date Create in 上午11:30 2018/6/25
 * @description
 */
@Getter
public enum ResultEnum {

    SUCCESS(100, "返回成功"),

    MISS_REQUEST_PARAM_ERROR(101, "缺少请求参数"),

    SAVE_USERNAME_EXIST_ERROR(102, "用户已存在！"),

    SAVE_CLIENT_EXIST_ERROR(103, "客户端已存在！"),

    PUBLIC_KEY_NOT_EXIST(104, "公钥不存在！"),

    FIND_USERNAME_NOT_EXIST(105, "用户不存在！"),

    FIND_CLIENT_NOT_EXIST(106, "客户端不存在！"),

    ;

    private Integer code;

    private String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
