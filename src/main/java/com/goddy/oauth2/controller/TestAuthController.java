package com.goddy.oauth2.controller;

import com.goddy.oauth2.common.util.ResultUtil;
import com.goddy.oauth2.model.VO.Result;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Goddy
 * @date Create in 下午4:39 2018/6/26
 * @description
 */
@RestController
@RequestMapping("/test")
public class TestAuthController {

    @RequestMapping("/meatball")
    public Result test() {
        return ResultUtil.success("肉丸子");
    }
}
