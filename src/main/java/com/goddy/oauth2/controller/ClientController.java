package com.goddy.oauth2.controller;

import com.goddy.oauth2.common.exception.MyException;
import com.goddy.oauth2.common.util.ResultUtil;
import com.goddy.oauth2.model.DO.Client;
import com.goddy.oauth2.model.DO.User;
import com.goddy.oauth2.model.VO.Result;
import com.goddy.oauth2.model.enums.Constant;
import com.goddy.oauth2.model.enums.ResultEnum;
import com.goddy.oauth2.service.MyClientDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Goddy
 * @date Create in 上午9:28 2018/6/26
 * @description
 */
@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MyClientDetailsService clientService;

    @RequestMapping("/create")
    public Client create(@RequestParam String clientId,
                         @RequestParam String resourceIds,
                         @RequestParam(required = false, defaultValue = "") String clientSecret,
                         @RequestParam String scope,
                         @RequestParam String authorities,
                         @RequestParam String authorizedGrantTypes,
                         @RequestParam(required = false, defaultValue = Constant.DEFAULT_ACCESS_TOKEN_VALIDITY) Integer accessTokenValidity,
                         @RequestParam(required = false, defaultValue = Constant.DEFAULT_REFRESH_TOKEN_VALIDITY) Integer refreshTokenValidity
    ) {
        Client client = new Client();
        client.setClientId(clientId);
        client.setResourceIds(resourceIds);
        client.setClientSecret(passwordEncoder.encode(clientSecret));
        client.setSecretRequire(!"".equals(clientSecret));
        client.setScope(scope);
        client.setScopeRequire(!"".equals(scope));
        client.setAuthorities(authorities);
        client.setAuthorizedGrantTypes(authorizedGrantTypes);
        client.setAccessTokenValidity(accessTokenValidity);
        client.setRefreshTokenValidity(refreshTokenValidity);
        return clientService.save(client);
    }

    @RequestMapping("/{clientId}")
    public Result<Client> findByName(@PathVariable String clientId) {
        Client client = clientService.findById(clientId);
        if (client == null) {
            throw new MyException(ResultEnum.FIND_CLIENT_NOT_EXIST);
        }
        return ResultUtil.success(client);
    }

    @RequestMapping("/clientId")
    public Result<Client> findByName2(@RequestParam String clientId) {
        Client client = clientService.findById(clientId);
        if (client == null) {
            throw new MyException(ResultEnum.FIND_CLIENT_NOT_EXIST);
        }
        return ResultUtil.success(client);
    }
}
