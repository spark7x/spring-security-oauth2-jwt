package com.goddy.oauth2.controller;

import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Goddy
 * @date Create in 上午9:58 2018/6/26
 * @description
 */
@RestController
@RequestMapping("/order")
public class TestController {

    @GetMapping("/test")
    public Map test(OAuth2Authentication auth) {

        Map<String, Object> result = new HashMap<>(3);

        OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) auth.getDetails();

        result.put("userId", auth.getName());
        result.put("userIP", details.getRemoteAddress());
        result.put("userRole", auth.getAuthorities());
        return result;
    }
}
