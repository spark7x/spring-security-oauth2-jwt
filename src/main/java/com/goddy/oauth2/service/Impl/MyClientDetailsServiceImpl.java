package com.goddy.oauth2.service.Impl;

import com.goddy.oauth2.common.exception.MyException;
import com.goddy.oauth2.model.DO.Client;
import com.goddy.oauth2.model.entity.MyClientPrincipal;
import com.goddy.oauth2.model.enums.ResultEnum;
import com.goddy.oauth2.repository.ClientRepository;
import com.goddy.oauth2.service.MyClientDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Service;

/**
 * @author Goddy
 * @date Create in 下午12:13 2018/6/25
 * @description
 */
@Service
public class MyClientDetailsServiceImpl implements MyClientDetailsService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        Client client = clientRepository.findByClientId(clientId);
        if (client == null) {
            throw new ClientRegistrationException(clientId);
        }
        return new MyClientPrincipal(client);
    }

    @Override
    public Client save(Client client) {

        //1、如果已存在，抛出异常
        if (clientRepository.findByClientId(client.getClientId()) != null) {
            throw new MyException(ResultEnum.SAVE_CLIENT_EXIST_ERROR);
        }

        //2、否则，添加
        return clientRepository.save(client);
    }

    @Override
    public Client findById(String clientId) {
        return clientRepository.findByClientId(clientId);
    }
}
