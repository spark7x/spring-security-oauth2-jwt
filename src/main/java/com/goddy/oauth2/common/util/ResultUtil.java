package com.goddy.oauth2.common.util;

import com.goddy.oauth2.model.VO.Result;
import com.goddy.oauth2.model.enums.ResultEnum;

/**
 * @author Goddy
 * @date Create in 上午11:31 2018/6/25
 * @description
 */
public class ResultUtil {

    public static <T> Result<T> success(T object) {
        Result<T> result = new Result<>();
        result.setData(object);
        result.setCode(ResultEnum.SUCCESS.getCode());
        result.setMsg(ResultEnum.SUCCESS.getMessage());
        return result;
    }

    public static Result success() {
        return success(null);
    }

    public static Result error(ResultEnum resultEnum) {
        Result result = new Result();
        result.setCode(resultEnum.getCode());
        result.setMsg(resultEnum.getMessage());
        return result;
    }

    public static Result error(Integer code, String message) {
        Result result = new Result();
        result.setCode(code);
        result.setMsg(message);
        return result;
    }
}
