package com.goddy.oauth2.common.exception;

import com.goddy.oauth2.model.enums.ResultEnum;
import lombok.Getter;

/**
 * @author Goddy
 * @date Create in 上午11:31 2018/6/25
 * @description
 */
@Getter
public class MyException extends RuntimeException {

    private Integer code;

    public MyException(ResultEnum resultEnum) {
        super(resultEnum.getMessage());
        this.code = resultEnum.getCode();
    }

    public MyException(Integer code, String msg) {
        super(msg);
        this.code = code;
    }

    public MyException(ResultEnum resultEnum, String msg) {
        super(resultEnum.getMessage().concat(msg));
        this.code = resultEnum.getCode();
    }
}
